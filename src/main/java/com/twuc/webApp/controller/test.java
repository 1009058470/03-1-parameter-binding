package com.twuc.webApp.controller;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController

public class test {
    @PostMapping("/api/student")
    String getName(@RequestBody @Valid Student student) {
        return student.getName();
    }
}
