package com.twuc.webApp.controller;

import javax.validation.constraints.NotNull;

public class Student{
    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeneral() {
        return general;
    }

    public void setGeneral(String general) {
        this.general = general;
    }

    private String general;
}
