package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/bind/int/{value}")
    public String testBindToIn(@PathVariable int value){
        return "Successful";
    }

    @GetMapping("/bind/{value}")
    public Integer testBindToInteger(@PathVariable Integer value){
        return value;
    }

    @GetMapping("/users/{userId}/books/{bookId} ")
    public String testBindToIntegerAndInt(@PathVariable Integer userId, @PathVariable int bookId){
        return "Successful";
    }

    @GetMapping("/getuser")
    public String getInfoByUserName(String username){
        return username;
    }

    @GetMapping("/getuserbydeafult")
    public String getInfoByDeafult(@RequestParam(defaultValue = "xiaoming") String username){
        return username;
    }

    @GetMapping("/bind/collection")
    public ArrayList<String> getCollection(@RequestParam ArrayList<String> myparam){
        System.out.println(myparam);
        return myparam;
    }

    @PostMapping("/datetimes")
    public LocalDate getDate(@RequestBody Mydate mydate){
        return mydate.getDateTime();
    }




}
