package com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.validation.Valid;

import java.util.ArrayList;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class pararmControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_bind_to_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/int/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }


    @Test
    void should_return_200_when_bind_to_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/15"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("15"));
    }


    @Test
    void should_return_200_when_bind_to_int_and_integer() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/15/books/15 "))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Successful"));
    }

    @Test
    void should_return_200_and_query_when_bind_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuser?username=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_return_200_and_query_user_deafult_query_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/getuserbydeafult"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("xiaoming"));
    }

    @Test
    void should_bind_collection_to_param() throws Exception {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("myValue1");
        arrayList.add("myValue2");
        arrayList.add("myValue3");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/bind/collection?myparam=myValue1&myparam=myValue2&myparam=myValue3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_date_when_query_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }


    @Test
    void should_return_date_when_query_incorrect_date_string() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes").content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("\"2019-10-01\""));
    }

    /********************************************************/
    @Test
    public void test_pararm_controller() throws Exception {
        // mockmvc.perform(get("/api/users/23")).andExpect();
    }


    @Test
    void test_notNull() throws Exception {
        mockMvc.perform(post("/api/student").content("{\"general\":\"female\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status()
                .isBadRequest());
    }

    @Test
    void test_for_json() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Student student = new Student();
        student.setName("xiaoMing");
        student.setGeneral("female");
        String student_json = objectMapper.writeValueAsString(student);
        System.out.println(student_json);
        // assertEquals(student_json,"{\"name\":\"xiaoming\",\"general\":\"female\"}");
    }

    @Test
    void test_for_post_json_return() throws Exception {
        mockMvc.perform(post("/api/student").content("{\"name\":\"xiaoming\",\"general\":\"female\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("xiaoming"));
    }
}
